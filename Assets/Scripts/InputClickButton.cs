using UnityEngine.Events;
using UnityEngine;

public class InputClickButton : MonoBehaviour {
    
    public UnityEvent onButtonDown;

    private void Update() {
        if(GvrControllerInput.ClickButtonDown){
            onButtonDown.Invoke();
        }
    }
}