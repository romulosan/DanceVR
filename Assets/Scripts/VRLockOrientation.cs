﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gvr;
using UnityEngine.XR;
using GoogleVR;

public class VRLockOrientation : MonoBehaviour {

	public float speed=1;
	public bool lockZ;
	public bool VR=true;

	private void Start() {
		XRSettings.enabled = VR;
		XRDevice.DisableAutoXRCameraTracking (GetComponent<Camera>(), true);
	}

	private void LateUpdate() {
		if(!Application.isMobilePlatform || Application.isEditor) return;
		Vector3 rot = InputTracking.GetLocalRotation(XRNode.CenterEye).eulerAngles;
		if(lockZ){
			rot.z = 0;
			transform.localRotation = Quaternion.Euler(rot);
		}else{
			float z = transform.localRotation.eulerAngles.z;
			rot.z = Mathf.LerpAngle(z,rot.z,speed*Time.deltaTime);
			transform.localRotation = Quaternion.Euler(rot);
		}
	}
}
