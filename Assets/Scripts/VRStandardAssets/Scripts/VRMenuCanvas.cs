﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VRMenuCanvas : MonoBehaviour {

    public Text uiText;
	private GameObject selectedGO;


    //private void OnGUI()
    //{
    //    KeyCode pressKey = KeyCode.None;
    //    Event dirEvent = Event.current;
    //    pressKey = dirEvent.keyCode;
    //    if (pressKey != KeyCode.None)
    //        uiText.text = pressKey.ToString();
    //}

    void Update () {
        if (!EventSystem.current.currentSelectedGameObject && selectedGO)
        {
            EventSystem.current.SetSelectedGameObject(selectedGO);
        }
        else if(EventSystem.current.currentSelectedGameObject)
        {
            selectedGO = EventSystem.current.currentSelectedGameObject;
        }
	}
}
