﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputEvent : MonoBehaviour {

    public KeyEvent[] inputEvents;

    [System.Serializable]
    public class KeyEvent {
        public KeyCode keyCode;
        public UnityEvent onKeyDown;

        public void GetKeyDown() {
            if (Input.GetKeyUp(keyCode)) {
                onKeyDown.Invoke();
            }
        }
    }
   
	void Update () {
        for (int i = 0; i < inputEvents.Length; i++)
        {
            inputEvents[i].GetKeyDown();
        }
	}
}
