﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Events;

public class TimelineSequence : MonoBehaviour {

	public int id;
	public OnPlayTimeline[] timelines;
	public UnityEvent onStart;
	public UnityEvent onEnd;

	[System.Serializable]
	public class OnPlayTimeline{
		public PlayableDirector playable;
		public UnityEvent onStart;
	}

	private void Awake() {
		onStart.Invoke();
	}

	public void Playnext(){
		if(id >= timelines.Length){
			onEnd.Invoke();
			return;
		}
		for (int i = 0; i < timelines.Length; i++)
		{
			timelines[i].playable.gameObject.SetActive(false);
		}
		timelines[id].playable.gameObject.SetActive(true);
		timelines[id].playable.Play();
		timelines[id].onStart.Invoke();
		id++;
	}

	public void RertartScene(){
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}

	public void ToogleTimeline(PlayableDirector timeline){
		if(timeline.state == PlayState.Paused){
			timeline.Resume();
		}else{
			timeline.Pause();
		}
	}

	public void TooglGameObject(GameObject obj){
		obj.SetActive(!obj.activeSelf);
	}
}
