using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Collections.Generic;

[TrackColor(0.5661765f, 0.777303f, 1f)]
[TrackClipType(typeof(AudioVolumeClip))]
[TrackBindingType(typeof(AudioSource))]
public class AudioVolumeTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<AudioVolumeMixerBehaviour>.Create (graph, inputCount);
    }

    // Please note this assumes only one component of type AudioSource on the same gameobject.
    public override void GatherProperties (PlayableDirector director, IPropertyCollector driver)
    {
#if UNITY_EDITOR
        AudioSource trackBinding = director.GetGenericBinding(this) as AudioSource;
        if (trackBinding == null)
            return;

        var serializedObject = new UnityEditor.SerializedObject (trackBinding);
        var iterator = serializedObject.GetIterator();
        while (iterator.NextVisible(true))
        {
            if (iterator.hasVisibleChildren)
                continue;

            driver.AddFromName<AudioSource>(iterator.propertyPath);
        }
#endif
        base.GatherProperties (director, driver);
    }
}
