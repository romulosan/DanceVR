using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AudioVolumeClip : PlayableAsset, ITimelineClipAsset
{
    public AudioVolumeBehaviour template = new AudioVolumeBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AudioVolumeBehaviour>.Create (graph, template);
        return playable;
    }
}
